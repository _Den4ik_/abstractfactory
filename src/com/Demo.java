package com;

import com.app.Application;
import com.factory.GUIFactory;
import com.factory.MacFactory;
import com.factory.WindowsFactory;

import java.util.Scanner;
/*
The user selects the type and creates the concreting factories dynamically
*/
public class Demo {
    private static Application configureApplication(String osName) {
        Application application;
        GUIFactory factory;
        if (osName.toLowerCase().contains("mac")) {
            factory = new MacFactory();
            application = new Application(factory);
        } else {
            factory = new WindowsFactory();
            application = new Application(factory);
        }
        return application;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Application app = configureApplication(scanner.next());
        app.paint();
    }

}
