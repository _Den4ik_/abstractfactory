package com.factory;

import com.Button;
import com.Checkbox;
import com.model.button.MacButton;
import com.model.checkbox.MacCheckbox;

public class MacFactory implements GUIFactory {
    @Override
    public Button createButton() {
        return new MacButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacCheckbox();
    }
}
