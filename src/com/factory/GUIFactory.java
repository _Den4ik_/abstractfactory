package com.factory;

import com.Button;
import com.Checkbox;

//Abstract factory
public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}
