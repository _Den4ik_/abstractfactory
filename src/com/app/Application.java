package com.app;

import com.Button;
import com.Checkbox;
import com.factory.GUIFactory;

public class Application {
    private Button button;
    private Checkbox checkbox;
    public Application(GUIFactory factory){
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }
    public void paint(){
        button.paint();
        checkbox.check();
    }
}
